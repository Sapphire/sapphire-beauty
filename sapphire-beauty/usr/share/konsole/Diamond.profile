[Appearance]
ColorScheme=Sapphire
Font=Fira Code,10,-1,5,50,0,0,0,0,0

[Cursor Options]
CursorShape=2

[General]
Command=/bin/zsh
Name=Diamond
Parent=FALLBACK/
TerminalMargin=10

[Scrolling]
HistoryMode=2
ScrollBarPosition=1

[Terminal Features]
BlinkingCursorEnabled=true
